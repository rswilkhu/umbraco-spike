A web project containing tests and sample usage of uMapper (part of uComponents, an add-on suite to Umbraco CMS). 

The test data is included as a SQL CE database - you don't have to install Umbraco, just build and run! 

The username and password for the back office are both "admin".