﻿using System.Collections.Generic;
using System.Linq;

using uComponents.Mapping;
using uMapperExample.Models;
using umbraco.NodeFactory;
using umbraco;
using System.Xml.Linq;

using umbraco.businesslogic;
using uComponents.Mapping.Property;

namespace uMapperExample
{
    public class ExampleStartupHandler : ApplicationStartupHandler
    {
        public ExampleStartupHandler()
        {
            uMapper.CreateMap<Homepage>("Site"); // override node type alias
            uMapper.CreateMap<Genre>();
            uMapper.CreateMap<Gig>();
            uMapper.CreateMap<HubPage>("view_HubPage");
            uMapper.CreateMap<BestBuy>("component_Bestbuy").CustomProperty(
                bb => bb.Title,
                (a, b, c) =>
                    {
                        var node = uQuery.GetCurrentNode();
                        return node.GetProperty<string>("title");
                }, false, true);
            

            // Maps a uComponents "Data Type Grid" to a non-relational collection
            CustomPropertyMapping reviewsMapping = (id, paths, cache) =>
            {
                var node = new Node(id); // the node will definitely exist when this mapping is run

                var xml = node.GetProperty<string>("reviews");

                if (string.IsNullOrWhiteSpace(xml))
                {
                    return new List<Artist.Review>();
                }

                return XElement.Parse(xml)
                    .Elements("item")
                    .Select(item => new Artist.Review
                    {
                        Reviewer = item.Element("reviewer").Value,
                        Body = item.Element("body").Value
                    })
                    .ToList();
            };


            uMapper.CreateMap<Artist>()
                .CustomProperty(
                    x => x.Reviews, // choose the model property
                    reviewsMapping, // set a mapping
                    false, // whether the mapping is deemed a 'relationship' with other nodes
                    true // allow the result of this mapping to be cached
                    );

            // SoloArtist inherits from Artist, as do the corresponding node types.
            // This map will inherit Artist's property mapping for Reviews.
            uMapper.CreateMap<SoloArtist>();
        }

        private object MapHubPageTitle(int nodeid, string[] paths, ICacheProvider cache)
        {
            var node = uQuery.GetCurrentNode();
            return node.GetProperty<string>("title");
        }
    }
}