﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uMapperExample.Models
{
    public class Artist
    {
        // Gets the node name & URL
        public string Name { get; set; }
        public string NiceUrl { get; set; }
        public int SortOrder { get; set; }
        public ArtistRating? Rating { get; set; }

        // You can use IEnumerable<> or List<>, or anything which implements
        // IEnumerable<> and has a constructor which takes a single IEnumerable<>
        // parameter.

        // Gets the descendants of node type alias "Gig", as there is
        // no "gigs" property on the node type.
        public IEnumerable<Gig> Gigs { get; set; }
            
        // Gets the Genre nodes selected with the Multi-Node Tree Picker.
        // They must be stored in CSV format to be automatically retrieved.
        public List<Genre> Genres { get; set; }

        // Complex types with custom mapping
        public IEnumerable<Review> Reviews { get; set; }

        public class Review
        {
            public string Reviewer { get; set; }
            public string Body { get; set; }
        }
    }

    public enum ArtistRating
    {
        Excellent = 45,
        Amazing = 46,
        Scrumdiddlyumptious = 48
    }
}