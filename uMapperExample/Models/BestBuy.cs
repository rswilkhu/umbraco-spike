namespace uMapperExample.Models
{
    public class BestBuy
    {
        public string Title { get; set; }

        public string FilterValue { get; set; }

        public string AnotherFilterValue { get; set; }
    }
}