﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uMapperExample.Models
{
    public class Genre
    {
        // Mirror the default Node properties (i.e. Name, DateCreated, Path, etc.)
        public string Name { get; set; }
    }
}
