﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace uMapperExample.Models
{
    public class Gig
    {
        public int Id { get; set; }
        public string Name { get; set; }

        // Gets the closest ancestor of the node type which
        // maps to Artist
        public Artist Artist { get; set; }

        // Mandatory date property
        public DateTime Date { get; set; }

        // Optional date property
        public DateTime? TicketsOnSale { get; set; }
    }
}
