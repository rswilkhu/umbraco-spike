﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace uMapperExample.Models
{
    // Mapped from the "Site" node type, see the overload of CreateMap()
    public class Homepage
    {
        public string Name { get; set; }
        public string NiceUrl { get; set; }
        public Artist FeaturedArtist { get; set; }
        public Genre FeaturedGenre { get; set; }
    }
}