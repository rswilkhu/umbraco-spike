﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace uMapperExample.Models
{
    // Create a mapping for Artist before SoloArtist and you'll inherit all of 
    // Artist's property mappings.
    public class SoloArtist : Artist
    {
        public string RealName { get; set; }
    }
}