﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uComponents.Mapping;
using Models = uMapperExample.Models;

namespace uMapperExample.masterpages
{
    public partial class Artist : System.Web.UI.MasterPage
    {
        protected Models.Homepage Home { get; set; }
        protected Models.Artist Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Model = uMapper.GetCurrent<Models.Artist>();
            Home = uMapper.GetAll<Models.Homepage>().Single();
        }
    }
}