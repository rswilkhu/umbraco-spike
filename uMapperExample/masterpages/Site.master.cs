﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using uComponents.Mapping;
using Models = uMapperExample.Models;
using umbraco;
using System.Text;
using System.Diagnostics;

namespace uMapperExample.masterpages
{
    public partial class Site : System.Web.UI.MasterPage
    {
        protected Models.Homepage Model { get; set; }
        protected IEnumerable<Models.Artist> Artists { get; set; }

        protected bool TestsPassed { get; set; }
        protected StringBuilder TestsLog { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            TestsLog = new StringBuilder();

            var tests = new uMapperTests();
            var legacyTests = new uMapperLegacyTests();

            legacyTests.RunTests(TestsLog, false);
            legacyTests.RunTests(TestsLog, true);

            tests.RunTests(TestsLog, false);
            tests.RunTests(TestsLog, true);

            tests.RunPerformanceTest(TestsLog, 50);

            TestsPassed = true;

            // Query examples:
            var home1 = uMapper.GetCurrent<Models.Homepage>(); // loads all relationships

            var home2 = uMapper.GetCurrent<Models.Homepage>(false); // doesn't load any relationships

            var home3 = uMapper.Query<Models.Homepage>()
                .Include(x => x.FeaturedArtist)
                .Current(); // only loads the FeaturedArtist relationship

            var home4 = uMapper.Find<Models.Homepage>(1057); // loads all relationships
            var home5 = uMapper.Find<Models.Homepage>(1057, false); // doesn't load any relationships

            var home6 = uMapper.Query<Models.Homepage>()
                .Include(x => x.FeaturedArtist.Gigs)
                .Find(1057); // loads the FeaturedArtist relationship, and populates that artist's gigs



            this.Model = uMapper.GetCurrent<Models.Homepage>();

            this.Artists = uMapper.Query<Models.Artist>()
                .Include(x => x.Genres)
                .OrderBy(artist => artist.SortOrder)
                .ToList();
        }
    }

}