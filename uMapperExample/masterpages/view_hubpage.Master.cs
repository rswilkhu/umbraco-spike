﻿using System;

namespace uMapperExample.masterpages
{
    using uComponents.Mapping;

    using uMapperExample.Models;

    public partial class view_hubpage : System.Web.UI.MasterPage
    {
        protected HubPage Model { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Model = uMapper.GetCurrent<HubPage>();
        }
    }
}