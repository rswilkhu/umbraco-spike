﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using uComponents.Mapping;
using uMapperExample.Models;
using umbraco.NodeFactory;
using umbraco;
using System.Diagnostics;

namespace uMapperExample
{
    public class uMapperLegacyTests
    {
        private StringBuilder _log;
        private INodeMappingEngine _engine = new NodeMappingEngine();

        public void Assert(bool condition)
        {
            if (!condition)
            {
                throw new TestsFailedException();
            }
        }

        public class TestsFailedException : Exception
        {
            public TestsFailedException()
            {
            }
        }

        public void RunTests(StringBuilder log, bool withCaching)
        {
            _log = log;

            if (withCaching)
            {
                _log.AppendLine("Enabling caching...");
                _engine.SetCacheProvider(new DefaultCacheProvider(HttpContext.Current.Cache));
            }
            else
            {
                _log.AppendLine("Disabling caching...");
                _engine.SetCacheProvider(null);
            }

            _log.AppendLine("Starting legacy tests...");

            _log.AppendLine("Testing basic property mapping...");
            TestBasicPropertyMapping();

            _log.AppendLine("Testing explicit relationships...");
            TestExplicitRelationships();

            _log.AppendLine("Testing descendant relationships...");
            TestDescendantRelationships();

            _log.AppendLine("Testing ancestor relationships...");
            TestAncestorRelationships();

            _log.AppendLine("Testing custom collections...");
            TestCustomCollection();

            _log.AppendLine("Testing remove mapping...");
            TestRemoveMapping();

            _log.AppendLine("Testing custom property mapping...");
            TestCustomPropertyMapping();

            _log.AppendLine("Testing relationships with IDs...");
            TestRelationshipsWithIds();

            _log.AppendLine("Testing specific inclusions of relationships...");
            TestSpecificRelationships();

            _log.AppendLine("Testing inheritance of mappings...");
            TestMappingInheritance();

            _log.AppendLine("Testing cast to base type...");
            TestCastingToBase();

            _log.AppendLine("Testing paths...");
            TestPaths();

            _log.AppendLine("Testing invalid paths...");
            TestInvalidPaths();

            _log.AppendLine("Testing query methods...");
            TestQueryMethods();

            _log.AppendLine("Testing property mappings with paths...");
            TestPropertyMappingsWithPaths();

            _log.AppendLine("Legacy tests passed");
        }

        // Suppress obsolete warnings
#pragma warning disable 612, 618

        private void TestBasicPropertyMapping()
        {
            _engine.CreateMap<Gig>();
            var model = _engine.Map<Gig>(new Node(1072), false);

            Assert(model.Id == 1072);
            Assert(model.Name == "Afrobeat @ Blue Beat");
            Assert(model.Date == new DateTime(2012, 10, 26));
            Assert(model.TicketsOnSale == null); // nullable
        }

        private void TestExplicitRelationships()
        {
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(new Node(1061), true);

            Assert(model != null);
            Assert(model.Genres != null);
            Assert(model.Genres.Count == 2);
            Assert(model.Genres.Any(genre => genre.Name == "Funk"));
        }

        private void TestDescendantRelationships()
        {
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(new Node(1061), true);

            Assert(model.Gigs != null);
            Assert(model.Gigs.Count() == 2);
            Assert(model.Gigs.Any(gig => gig.Id == 1072));
        }

        private void TestAncestorRelationships()
        {
            _engine.CreateMap<Artist>();
            _engine.CreateMap<Gig>();
            var model = _engine.Map<Gig>(new Node(1072), true);

            Assert(model.Artist != null);
            Assert(model.Artist.Name == "Kooii");
            Assert(model.Artist.Gigs == null); // relationships should not be mapped
        }

        private void TestCustomCollection()
        {
            _engine.CreateMap<ArtistWithCustomCollection>("Artist");
            var model = _engine.Map<ArtistWithCustomCollection>(new Node(1061), true);

            Assert(model.Genres != null);
            Assert(model.Genres.Count() == 2);
            Assert(model.Genres.First().Name == "Funk");
        }

        private class ArtistWithCustomCollection
        {
            public CustomList<Genre> Genres { get; set; }

            public class CustomList<T> : IEnumerable<T>
            {
                private List<T> _innerList;

                public CustomList(IEnumerable<T> collection)
                {
                    _innerList = new List<T>(collection);
                }

                public IEnumerator<T> GetEnumerator()
                {
                    return _innerList.GetEnumerator();
                }

                System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
                {
                    return _innerList.GetEnumerator();
                }
            }
        }

        private void TestRemoveMapping()
        {
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Name)
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(new Node(1061), true);

            Assert(model.Name == null);
        }

        private void TestCustomPropertyMapping()
        {
            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Name, n => "Fred", false)
                .RemoveMappingForProperty(x => x.Reviews);

            var model = _engine.Map<Artist>(new Node(1061), false);

            Assert(model.Name == "Fred");

            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Genres, n => new List<Genre>(), true)
                .RemoveMappingForProperty(x => x.Reviews);

            var modelRelationship = _engine.Map<Artist>(new Node(1061), true);

            Assert(modelRelationship.Genres != null);
            Assert(modelRelationship.Genres.Count == 0);
        }

        private void TestRelationshipsWithIds()
        {
            _engine.CreateMap<ArtistWithIds>("Artist");
            var model = _engine.Map<ArtistWithIds>(new Node(1061), true);

            Assert(model.Genres != null);
            Assert(model.Genres.Count == 2);
            Assert(model.Genres.Any(x => x == 1066));
        }

        private class ArtistWithIds
        {
            public int Site { get; set; }
            public List<int> Gigs { get; set; }
            public List<int> Genres { get; set; }
        }

        private void TestSpecificRelationships()
        {
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(
                new Node(1061),
                x => x.Genres
                );

            Assert(model != null);
            Assert(model.Genres != null);
            Assert(model.Genres.Count == 2);
            Assert(model.Gigs == null);

            // Ancestor relationship
            var gig = _engine.Map<Gig>(new Node(1072));

            Assert(gig != null);
            Assert(gig.Artist == null); // excluded

            // Missing relationship
            var didFailCorrectly2 = false;
            try
            {
                var model3 = _engine.Map<Artist>(
                    new Node(1061),
                    x => x.Reviews // relationship not mapped
                    );
            }
            catch (InvalidPathException)
            {
                didFailCorrectly2 = true;
            }

            Assert(didFailCorrectly2);

            // Invalid relationship
            var didFailCorrectly = false;
            try
            {
                _engine.Map<Artist>(
                    new Node(1061),
                    x => x.Name // not a relationship
                    );
            }
            catch (InvalidPathException)
            {
                didFailCorrectly = true;
            }

            Assert(didFailCorrectly);
        }

        private void TestMappingInheritance()
        {
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Name, n => "Overridden", false)
                .ForProperty(x => x.Genres, n => new List<Genre>(), true);

            _engine.CreateMap<SoloArtist>();

            var laneous = _engine.Map<SoloArtist>(new Node(1060), x => x.Genres);
            Assert(laneous.Name == "Overridden");
            Assert(laneous.Genres.Count == 0);

            var otherLaneous = _engine.Map<Artist>(new Node(1060), x => x.Genres);
            Assert(otherLaneous.Name == "Overridden");
            Assert(otherLaneous.Genres.Count == 0);

            var wrongNode = _engine.Map<Genre>(new Node(1060), false);
            Assert(wrongNode == null);
        }

        private void TestCastingToBase()
        {
            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Name, n => "Overridden", false);

            _engine.CreateMap<SoloArtist>()
                .ForProperty(x => x.RealName, "firstName");

            var laneous = _engine.Map<SoloArtist>(new Node(1060), false);
            Assert(laneous.RealName == "Lachlan");

            var otherLaneous = _engine.Map<Artist>(new Node(1060), false);
            Assert((otherLaneous as SoloArtist).RealName == "Lachlan");

            // Try map Artist to SoloArtist
            var wrongNode = _engine.Map<SoloArtist>(new Node(1061), false);
            Assert(wrongNode == null);
        }

        private void TestPaths()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Reviews, (n, p) => new List<Artist.Review>(), true);
            _engine.CreateMap<SoloArtist>();

            var laneous = _engine.Query<Artist>()
                .Include("Genres")
                .Include("Gigs.Artist.Reviews") // down/up/down the tree
                .Map(new Node(1060));

            Assert(laneous != null);
            Assert(laneous.Name == "Laneous & The Family Yah");
            Assert(laneous.Reviews == null); // not specified
            Assert(laneous.Genres != null); // single level path
            Assert(laneous.Genres.Count() == 4);
            Assert(laneous.Gigs != null); // first level of double level path
            Assert(laneous.Gigs.Count() == 2);
            Assert(laneous.Gigs.First().Id == 1070);
            Assert(laneous.Gigs.First().Artist != null); // second level
            Assert(laneous.Gigs.First().Artist.Name == "Laneous & The Family Yah");
            Assert(laneous.Gigs.First().Artist.Reviews != null); // third level

            /* Test with lambdas */
            var yah = _engine.Query<Artist>()
                .Include(x => x.Genres)
                .Include(x => x.Gigs.Select(y => y.Artist.Reviews)) // down/up/down the tree
                .Map(new Node(1060));

            Assert(yah != null);
            Assert(yah.Name == "Laneous & The Family Yah");
            Assert(yah.Reviews == null); // not specified
            Assert(yah.Genres != null); // single level path
            Assert(yah.Genres.Count() == 4);
            Assert(yah.Gigs != null); // first level of double level path
            Assert(yah.Gigs.Count() == 2);
            Assert(yah.Gigs.First().Id == 1070);
            Assert(yah.Gigs.First().Artist != null); // second level
            Assert(yah.Gigs.First().Artist.Name == "Laneous & The Family Yah");
            Assert(yah.Gigs.First().Artist.Reviews != null); // third level
        }

        private void TestInvalidPaths()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Reviews, (n, p) => new List<Artist.Review>(), true);
            _engine.CreateMap<SoloArtist>();

            var invalidPaths = new string[] {
                "RealName",
                "Name",
                "Gigs.Date",
                "Gigs_Artist",
                ".agf438earioa",
                "4a.t4.a.gra.aegt"
            };

            foreach (var invalidPath in invalidPaths)
            {
                var didFailCorrectly = false;
                try
                {
                    _engine.Query<SoloArtist>()
                        .Include(invalidPath)
                        .Single(1060);
                }
                catch (InvalidPathException)
                {
                    didFailCorrectly = true;
                }

                Assert(didFailCorrectly);
            }
        }

        private void TestQueryMethods()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .ForProperty(x => x.Reviews, (n, p) => new List<Artist.Review>(), true);
            _engine.CreateMap<SoloArtist>();

            // Map
            var a = _engine.Query<Artist>()
                .Map(new Node(1060));

            Assert(a != null);
            Assert(a is SoloArtist);
            Assert(a.Gigs == null);
            Assert(a.Name == "Laneous & The Family Yah");

            // Single
            var b = _engine.Query<Artist>()
                .Single(1060);

            Assert(b != null);
            Assert(b is SoloArtist);
            Assert(b.Gigs == null);
            Assert(b.Name == "Laneous & The Family Yah");

            // Many
            var c = _engine.Query<Artist>()
                .Many(new[] { 
                    1060,
                    1061
                });

            Assert(c != null);
            Assert(c.Count() == 2);
            Assert(c.Any(x => x.Name == "Laneous & The Family Yah"));
            Assert(!c.Any(x => x.Gigs != null));

            // Many nodes
            var d = _engine.Query<Artist>()
                .Many(new[] { 
                    new Node(1060),
                    new Node(9999)
                });

            Assert(d != null);
            Assert(d.Count() == 2);
            Assert(d.Any(x => x.Name == "Laneous & The Family Yah"));
            Assert(d.Where(x => x == null).Count() == 1);

            // All
            var e = _engine.Query<Artist>();

            Assert(e != null);
            Assert(e.Count() == 4);
            Assert(!e.Any(x => x == null));

            // All explicit
            var f = _engine.Query<Artist>()
                .Explicit();

            Assert(f != null);
            Assert(f.Count() == 2);
            Assert(!f.Any(x => (x is SoloArtist)));
        }

        private void TestPropertyMappingsWithPaths()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>();
            _engine.CreateMap<SoloArtist>()
                .ForProperty(
                    x => x.Gigs, 
                    (n, p) =>
                        {
                            var nodes = n.GetDescendantNodesByType("Gig");

                            return _engine.Query<Gig>()
                                .IncludeMany(p)
                                .Many(nodes);
                        }, 
                    true);

            var laneous = _engine.Query<SoloArtist>()
                .Include(x => x.Gigs.Select(y => y.Artist))
                .Single(1060);

            Assert(laneous != null);
            Assert(laneous.Gigs.Count() == 2);
            Assert(!laneous.Gigs.Any(x => x.Artist == null));
        }


        /* Need to reimplement the below */
        //private void TestPropertyMappingChange()
        //{
        //    _log.AppendLine("    Creating map...");

        //    var config = _mapper.CreateMap<StringsOnly>();

        //    var defaultModel = _mapper.Map<StringsOnly>(new Node(1045));

        //    config.ForProperty(x => x.PhoneNumber, n => n.UrlName, false);

        //    var modifiedModel = _mapper.Map<StringsOnly>(new Node(1045));

        //    Assert(modifiedModel.Name == "Strings Only");
        //    Assert(modifiedModel.PhoneNumber == "strings-only");

        //    _log.AppendLine("    Success!");
        //}

        //private void TestInvalidMapException()
        //{
        //    _log.AppendLine("    Creating map...");
        //    _mapper.CreateMap<SingleRelationshipInvalidCast>("SingleRelationship");

        //    bool didFailCorrectly = false;
        //    try
        //    {
        //        var model = _mapper.Get<SingleRelationshipInvalidCast>(1060, true);
        //    }
        //    catch (MapNotFoundException)
        //    {
        //        didFailCorrectly = true;
        //    }

        //    Assert(didFailCorrectly);

        //    _log.AppendLine("    Success!");
        //}

        //private void TestDefaultNodeProperties()
        //{
        //    _log.AppendLine("    Creating map...");
        //    _mapper.CreateMap<DefaultNodeProperties>("SystemTypesOnly");

        //    var node = new Node(1049);
        //    var model = _mapper.Map<DefaultNodeProperties>(node);

        //    Assert(model.CreateDate == new DateTime(2012, 10, 17, 17, 9, 21));
        //    Assert(model.CreatorID == 0);
        //    Assert(model.CreatorName == "admin");
        //    Assert(model.Id == 1049);
        //    Assert(model.Level == 1);
        //    Assert(model.Name == "System Types Only Full");
        //    Assert(model.NodeTypeAlias == "SystemTypesOnly");
        //    Assert(model.Path == "-1,1049");
        //    Assert(model.SortOrder == 3);
        //    Assert(model.template == 0);
        //    Assert(model.UpdateDate < DateTime.Now);
        //    Assert(model.Url == "/system-types-only-full/");
        //    Assert(model.UrlName == "system-types-only-full");
        //    Assert(model.Version == new Guid());
        //    Assert(model.WriterID == 0);
        //    Assert(model.WriterName == "admin");

        //    _log.AppendLine("    Success!");
        //}

        //private void TestNullMap()
        //{
        //    var model = _mapper.Map<StringsOnly>(null);

        //    Assert(model == null);

        //    _log.AppendLine("    Success!");
        //}

        //private void TestUnknownDoctype()
        //{
        //    var didFailCorrectly = false;

        //    try
        //    {
        //        _mapper.CreateMap<NotMapped>("Whatever");
        //    }
        //    catch (DocumentTypeNotFoundException)
        //    {
        //        didFailCorrectly = true;
        //    }

        //    Assert(didFailCorrectly);

        //    _log.AppendLine("    Success!");
        //}

        //private void TestMissingMap()
        //{
        //    var didFailCorrectly = false;

        //    try
        //    {
        //        _mapper.Map<NotMapped>(new Node(1061));
        //    }
        //    catch (MapNotFoundException)
        //    {
        //        didFailCorrectly = true;
        //    }

        //    Assert(didFailCorrectly);

        //    _log.AppendLine("    Success!");
        //}

        // Restore obsolete warnings
#pragma warning restore 612, 618
    }
}