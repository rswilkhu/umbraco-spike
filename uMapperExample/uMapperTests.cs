﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using uComponents.Mapping;
using uMapperExample.Models;
using umbraco.NodeFactory;
using umbraco;
using System.Diagnostics;
using uComponents.Mapping.Property;
using System.Xml.Linq;

namespace uMapperExample
{
    public class uMapperTests
    {
        private StringBuilder _log;
        private INodeMappingEngine _engine = new NodeMappingEngine();

        [DebuggerStepThrough]
        public void Assert(bool condition)
        {
            if (!condition)
            {
                throw new TestsFailedException();
            }
        }

        public class TestsFailedException : Exception
        {
            public TestsFailedException()
            {
            }
        }

        public void RunTests(StringBuilder log, bool withCaching)
        {
            _log = log;

            if (withCaching)
            {
                _log.AppendLine("Enabling caching...");
                _engine.SetCacheProvider(new DefaultCacheProvider(HttpContext.Current.Cache));
            }
            else
            {
                _log.AppendLine("Disabling caching...");
                _engine.SetCacheProvider(null);
            }

            _log.AppendLine("Starting tests...");

            _log.AppendLine("Testing basic property mapping...");
            TestSimpleMapping();

            _log.AppendLine("Testing explicit relationships...");
            TestExplicitRelationships();

            _log.AppendLine("Testing descendant relationships...");
            TestDescendantRelationships();

            _log.AppendLine("Testing ancestor relationships...");
            TestAncestorRelationships();

            _log.AppendLine("Testing custom collections...");
            TestCustomCollection();

            _log.AppendLine("Testing remove mapping...");
            TestRemoveMapping();

            _log.AppendLine("Testing relationships with IDs...");
            TestRelationshipsWithIds();

            _log.AppendLine("Testing specific inclusions of relationships...");
            TestSpecificRelationships();

            _log.AppendLine("Testing inheritance of mappings...");
            TestMappingInheritance();

            _log.AppendLine("Testing cast to base type...");
            TestCastingToBase();

            _log.AppendLine("Testing paths...");
            TestPaths();

            _log.AppendLine("Testing invalid paths...");
            TestInvalidPaths();

            _log.AppendLine("Testing query methods...");
            TestQueryMethods();

            _log.AppendLine("Testing property mappings with paths...");
            TestPropertyMappingsWithPaths();

            _log.AppendLine("Testing property query methods...");
            TestPropertyQueryMethods();

            _log.AppendLine("Testing default property mapping...");
            TestDefaultPropertyMapping();

            _log.AppendLine("Testing basic property mapping...");
            TestBasicPropertyMapping();

            _log.AppendLine("Testing single property mapping...");
            TestSinglePropertyMapping();

            _log.AppendLine("Testing collection property mapping...");
            TestCollectionPropertyMapping();

            _log.AppendLine("Testing custom property mapping...");
            TestCustomPropertyMapping();

            _log.AppendLine("Enabling caching...");

            _engine.SetCacheProvider(new DefaultCacheProvider(HttpContext.Current.Cache));

            // TODO caching tests

            _log.AppendLine("Tests passed");
        }

        private void TestSimpleMapping()
        {
            _engine.CreateMap<Gig>();
            var model = _engine.Map<Gig>(new Node(1072), new string[0]);

            Assert(model.Id == 1072);
            Assert(model.Name == "Afrobeat @ Blue Beat");
            Assert(model.Date == new DateTime(2012, 10, 26));
            Assert(model.TicketsOnSale == null); // nullable
        }

        private void TestExplicitRelationships()
        {
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(new Node(1061), (string[])null);

            Assert(model != null);
            Assert(model.Genres != null);
            Assert(model.Genres.Count == 2);
            Assert(model.Genres.Any(genre => genre.Name == "Funk"));
        }

        private void TestDescendantRelationships()
        {
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(new Node(1061), (string[])null);

            Assert(model.Gigs != null);
            Assert(model.Gigs.Count() == 2);
            Assert(model.Gigs.Any(gig => gig.Id == 1072));
        }

        private void TestAncestorRelationships()
        {
            _engine.CreateMap<Artist>();
            _engine.CreateMap<Gig>();
            var model = _engine.Map<Gig>(new Node(1072), (string[])null);

            Assert(model.Artist != null);
            Assert(model.Artist.Name == "Kooii");
            Assert(model.Artist.Gigs == null); // relationships should not be mapped
        }

        private void TestCustomCollection()
        {
            _engine.CreateMap<ArtistWithCustomCollection>("Artist");
            var model = _engine.Map<ArtistWithCustomCollection>(new Node(1061), (string[])null);

            Assert(model.Genres != null);
            Assert(model.Genres.Count() == 2);
            Assert(model.Genres.First().Name == "Funk");
        }

        private class ArtistWithCustomCollection
        {
            public CustomList<Genre> Genres { get; set; }

            public class CustomList<T> : IEnumerable<T>
            {
                private List<T> _innerList;

                public CustomList(IEnumerable<T> collection)
                {
                    _innerList = new List<T>(collection);
                }

                public IEnumerator<T> GetEnumerator()
                {
                    return _innerList.GetEnumerator();
                }

                System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
                {
                    return _innerList.GetEnumerator();
                }
            }
        }

        private void TestRemoveMapping()
        {
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Name)
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(new Node(1061), (string[])null);

            Assert(model.Name == null);
        }

        private void TestRelationshipsWithIds()
        {
            _engine.CreateMap<ArtistWithIds>("Artist");
            var model = _engine.Map<ArtistWithIds>(new Node(1061), (string[])null);

            Assert(model.Genres != null);
            Assert(model.Genres.Count == 2);
            Assert(model.Genres.Any(x => x == 1066));
        }

        private class ArtistWithIds
        {
            public int Site { get; set; }
            public List<int> Gigs { get; set; }
            public List<int> Genres { get; set; }
        }

        private void TestSpecificRelationships()
        {
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            var model = _engine.Map<Artist>(
                new Node(1061),
                "Genres"
                );

            Assert(model != null);
            Assert(model.Genres != null);
            Assert(model.Genres.Count == 2);
            Assert(model.Gigs == null);

            // Ancestor relationship
            var gig = _engine.Map<Gig>(new Node(1072), new string[0]);

            Assert(gig != null);
            Assert(gig.Artist == null); // excluded

            // Missing relationship
            var didFailCorrectly2 = false;
            try
            {
                var model3 = _engine.Map<Artist>(
                    new Node(1061),
                    "Reviews" // relationship not mapped
                    );
            }
            catch (InvalidPathException)
            {
                didFailCorrectly2 = true;
            }

            Assert(didFailCorrectly2);

            // Invalid relationship
            var didFailCorrectly = false;
            try
            {
                _engine.Map<Artist>(
                    new Node(1061),
                    "Name" // not a relationship
                    );
            }
            catch (InvalidPathException)
            {
                didFailCorrectly = true;
            }

            Assert(didFailCorrectly);
        }

        private void TestMappingInheritance()
        {
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .DefaultProperty(x => x.Name, n => n.Name, p => "Overridden")
                .CollectionProperty<IEnumerable<int>>(x => x.Genres, p => new int[0]);

            _engine.CreateMap<SoloArtist>();

            var laneous = _engine.Map<SoloArtist>(new Node(1060), "Genres");
            Assert(laneous.Name == "Overridden");
            Assert(laneous.Genres.Count == 0);

            var otherLaneous = _engine.Map<Artist>(new Node(1060), "Genres");
            Assert(otherLaneous.Name == "Overridden");
            Assert(otherLaneous.Genres.Count == 0);

            var wrongNode = _engine.Map<Genre>(new Node(1060), new string[0]);
            Assert(wrongNode == null);
        }

        private void TestCastingToBase()
        {
            _engine.CreateMap<Artist>()
                .DefaultProperty(x => x.Name, n => n.Name, p => "Overridden");

            _engine.CreateMap<SoloArtist>()
                .BasicProperty(x => x.RealName, "firstName");

            var laneous = _engine.Map<SoloArtist>(new Node(1060), new string[0]);
            Assert(laneous.RealName == "Lachlan");

            var otherLaneous = _engine.Map<Artist>(new Node(1060), new string[0]);
            Assert((otherLaneous as SoloArtist).RealName == "Lachlan");

            // Try map Artist to SoloArtist
            var wrongNode = _engine.Map<SoloArtist>(new Node(1061), new string[0]);
            Assert(wrongNode == null);
        }

        private void TestPaths()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .CollectionProperty<IEnumerable<int>>(x => x.Reviews, p => new int[0]);
            _engine.CreateMap<SoloArtist>();

            var laneous = _engine.Query<Artist>()
                .Include("Genres")
                .Include("Gigs.Artist.Reviews") // down/up/down the tree
                .Map(new Node(1060));

            Assert(laneous != null);
            Assert(laneous.Name == "Laneous & The Family Yah");
            Assert(laneous.Reviews == null); // not specified
            Assert(laneous.Genres != null); // single level path
            Assert(laneous.Genres.Count() == 4);
            Assert(laneous.Gigs != null); // first level of double level path
            Assert(laneous.Gigs.Count() == 2);
            Assert(laneous.Gigs.First().Id == 1070);
            Assert(laneous.Gigs.First().Artist != null); // second level
            Assert(laneous.Gigs.First().Artist.Name == "Laneous & The Family Yah");
            Assert(laneous.Gigs.First().Artist.Reviews != null); // third level

            /* Test with lambdas */
            var yah = _engine.Query<Artist>()
                .Include(x => x.Genres)
                .Include(x => x.Gigs.Select(y => y.Artist.Reviews)) // down/up/down the tree
                .Map(new Node(1060));

            Assert(yah != null);
            Assert(yah.Name == "Laneous & The Family Yah");
            Assert(yah.Reviews == null); // not specified
            Assert(yah.Genres != null); // single level path
            Assert(yah.Genres.Count() == 4);
            Assert(yah.Gigs != null); // first level of double level path
            Assert(yah.Gigs.Count() == 2);
            Assert(yah.Gigs.First().Id == 1070);
            Assert(yah.Gigs.First().Artist != null); // second level
            Assert(yah.Gigs.First().Artist.Name == "Laneous & The Family Yah");
            Assert(yah.Gigs.First().Artist.Reviews != null); // third level
        }

        private void TestInvalidPaths()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .BasicProperty<string>(x => x.Reviews, p => new Artist.Review[0]);
            _engine.CreateMap<SoloArtist>();

            var invalidPaths = new string[] {
                "RealName",
                "Name",
                "Gigs.Date",
                "Gigs_Artist",
                ".agf438earioa",
                "4a.t4.a.gra.aegt"
            };

            foreach (var invalidPath in invalidPaths)
            {
                var didFailCorrectly = false;
                try
                {
                    _engine.Query<SoloArtist>()
                        .Include(invalidPath)
                        .Find(1060);
                }
                catch (InvalidPathException)
                {
                    didFailCorrectly = true;
                }

                Assert(didFailCorrectly);
            }
        }

        private void TestQueryMethods()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .BasicProperty<string>(x => x.Reviews, p => new Artist.Review[0]);
            _engine.CreateMap<SoloArtist>();

            // Map
            var a = _engine.Query<Artist>()
                .Map(new Node(1060));

            Assert(a != null);
            Assert(a is SoloArtist);
            Assert(a.Gigs == null);
            Assert(a.Name == "Laneous & The Family Yah");

            // Single
            var b = _engine.Query<Artist>()
                .Find(1060);

            Assert(b != null);
            Assert(b is SoloArtist);
            Assert(b.Gigs == null);
            Assert(b.Name == "Laneous & The Family Yah");

            // Many
            var c = _engine.Query<Artist>()
                .Many(new[] { 
                    1060,
                    1061
                });

            Assert(c != null);
            Assert(c.Count() == 2);
            Assert(c.Any(x => x.Name == "Laneous & The Family Yah"));
            Assert(!c.Any(x => x.Gigs != null));

            // Many nodes
            var d = _engine.Query<Artist>()
                .Many(new[] { 
                    new Node(1060),
                    new Node(9999)
                });

            Assert(d != null);
            Assert(d.Count() == 2);
            Assert(d.Any(x => x.Name == "Laneous & The Family Yah"));
            Assert(d.Where(x => x == null).Count() == 1);

            // All
            var e = _engine.Query<Artist>();

            Assert(e != null);
            Assert(e.Count() == 4);
            Assert(!e.Any(x => x == null));

            // All explicit
            var f = _engine.Query<Artist>()
                .Explicit();

            Assert(f != null);
            Assert(f.Count() == 2);
            Assert(!f.Any(x => (x is SoloArtist)));
        }

        private void TestPropertyMappingsWithPaths()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>();
            _engine.CreateMap<SoloArtist>()
                .CustomProperty(
                    x => x.Gigs,
                    (id, paths, cache) =>
                    {
                        var nodes = new Node(id).GetDescendantNodesByType("Gig");

                        return _engine.Query<Gig>()
                            .IncludeMany(paths)
                            .Many(nodes);
                    },
                    true,
                    true
                    );

            var laneous = _engine.Query<SoloArtist>()
                .Include(x => x.Gigs.Select(y => y.Artist))
                .Find(1060);

            Assert(laneous != null);
            Assert(laneous.Gigs.Count() == 2);
            Assert(!laneous.Gigs.Any(x => x.Artist == null));
        }

        private void TestPropertyQueryMethods()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Genre>();
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            _engine.CreateMap<SoloArtist>()
                .RemoveMappingForProperty(x => x.Reviews);

            // Select property
            var names = _engine.Query<Artist>()
                .SelectProperty(x => x.Name);

            Assert(names != null);
            Assert(names.Count() == 4);

            var explicitNames = _engine.Query<Artist>()
                .Explicit()
                .SelectProperty(x => x.Name);

            Assert(explicitNames != null);
            Assert(explicitNames.Count() == 2);

            // Select relationships
            var gigs = _engine.Query<Artist>()
                .SelectProperty(x => x.Gigs);

            // have not included Gigs in query, but should still get them
            Assert(gigs != null);
            Assert(gigs.Sum(x => x.Count()) == 5);

            // Test failure
            var didFailCorrectly = false;
            try
            {
                // "Reviews" has not been mapped
                _engine.Query<Artist>()
                    .SelectProperty(x => x.Reviews);
            }
            catch (PropertyNotMappedException)
            {
                didFailCorrectly = true;
            }

            Assert(didFailCorrectly);

            // Property filtering
            var earlyArtists = _engine.Query<Artist>()
                .WhereProperty(x => x.SortOrder, p => p < 2)
                .ToList();

            Assert(earlyArtists != null);
            Assert(earlyArtists.Count == 2);

            var activeArtists = _engine.Query<Artist>()
                .WhereProperty(x => x.Gigs, p => p.Any())
                .ToList();

            // gigs were not included but should work anyways
            Assert(activeArtists != null);
            Assert(activeArtists.Count == 3);

            var activeGroups = _engine.Query<Artist>()
                .Explicit()
                .WhereProperty(x => x.Gigs, p => p.Any())
                .ToList();

            Assert(activeGroups != null);
            Assert(activeGroups.Count == 1);

            var didFailCorrectly2 = false;
            try
            {
                _engine.Query<Artist>()
                    .WhereProperty(x => x.Reviews, p => p.Any());
                // Should fail before enumeration occurs
            }
            catch (PropertyNotMappedException)
            {
                didFailCorrectly2 = true;
            }

            Assert(didFailCorrectly2);
        }

        private void TestDefaultPropertyMapping()
        {
            _engine.CreateMap<Genre>()
                .DefaultProperty(
                    x => x.Name,
                    n => n.WriterName,
                    p => p.ToUpper()
                    );

            var genre = _engine.Query<Genre>()
                .ToList();

            Assert(!genre.Any(x => x.Name != "ADMIN"));
        }

        private void TestBasicPropertyMapping()
        {
            _engine.CreateMap<Gig>()
                .BasicProperty(x => x.TicketsOnSale, "date");

            var gig1 = _engine.Query<Gig>()
                .WhereProperty(x => x.Id, id => id == 1072)
                .Single();

            Assert(gig1.TicketsOnSale.HasValue);
            Assert(gig1.TicketsOnSale.Value == new DateTime(2012, 10, 26));

            _engine.CreateMap<Gig>()
                .BasicProperty<DateTime?>(
                    x => x.TicketsOnSale,
                    p => p.HasValue ? p.Value : DateTime.MinValue
                    );

            var gig2 = _engine.Query<Gig>()
                .WhereProperty(x => x.Id, id => id == 1072)
                .Single();

            Assert(gig2.TicketsOnSale.HasValue);
            Assert(gig2.TicketsOnSale.Value == DateTime.MinValue);

            _engine.CreateMap<Gig>()
                .BasicProperty<DateTime>(
                    x => x.TicketsOnSale,
                    p => p.AddDays(1),
                    "date"
                    );

            var gig3 = _engine.Query<Gig>()
                .WhereProperty(x => x.Id, id => id == 1072)
                .Single();

            Assert(gig3.TicketsOnSale.HasValue);
            Assert(gig3.TicketsOnSale.Value == new DateTime(2012, 10, 27));
        }

        private void TestSinglePropertyMapping()
        {
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews);
            _engine.CreateMap<SoloArtist>()
                .RemoveMappingForProperty(x => x.Reviews);
            _engine.CreateMap<Homepage>("Site")
                .SingleProperty(x => x.FeaturedArtist, "otherFeaturedArtist");

            var homeNotIncluded = _engine.Query<Homepage>()
                .Single();

            Assert(homeNotIncluded.FeaturedArtist == null);

            var homeIncluded = _engine.Query<Homepage>()
                .Include(x => x.FeaturedArtist)
                .Single();

            Assert(homeIncluded.FeaturedArtist != null);
            Assert(homeIncluded.FeaturedArtist.Name == "Alphamama");

            _engine.CreateMap<Homepage>("Site")
                .SingleProperty<string>(
                    x => x.FeaturedArtist,
                    p =>
                    {
                        var artistNodes = uQuery.GetNodesByType("Artist").Concat(uQuery.GetNodesByType("SoloArtist"));
                        var match = artistNodes
                            .Where(x => x.Name.ToLower().Contains(p.ToLower()))
                            .FirstOrDefault();
                        return match == null ? (int?)null : match.Id;
                    },
                    "artistSearch"
                    );

            var homeWithSearch = _engine.Query<Homepage>()
                .Include(x => x.FeaturedArtist)
                .Single();

            Assert(homeWithSearch.FeaturedArtist != null);
            Assert(homeWithSearch.FeaturedArtist.Name == "Ungus Ungus Ungus");

            _engine.CreateMap<Homepage>("Site")
                .SingleProperty(
                    x => x.FeaturedArtist,
                    id => 1064
                    );

            var homeWithUngus = _engine.Query<Homepage>()
                .Include(x => x.FeaturedArtist)
                .Single();

            Assert(homeWithSearch.FeaturedArtist != null);
            Assert(homeWithSearch.FeaturedArtist.Name == "Ungus Ungus Ungus");
        }

        private void TestCollectionPropertyMapping()
        {
            _engine.CreateMap<Gig>();
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews)
                .CollectionProperty(
                    x => x.Gigs,
                    "gigYouShouldGoToAfter"
                    );
            _engine.CreateMap<SoloArtist>()
                .RemoveMappingForProperty(x => x.Reviews);

            var laneous = _engine.Query<Artist>()
                .Include(x => x.Gigs)
                .Find(1060);

            Assert(laneous.Gigs != null);
            Assert(laneous.Gigs.Count() == 1);
            Assert(laneous.Gigs.First().Id == 1073);

            _engine.CreateMap<Artist>()
                .CollectionProperty<IEnumerable<int>>(
                    x => x.Gigs,
                    ids => ids.Take(1)
                    );
            _engine.CreateMap<SoloArtist>()
                .RemoveMappingForProperty(x => x.Reviews);

            var laneous2 = _engine.Query<Artist>()
                .Include(x => x.Gigs)
                .Find(1060);

            Assert(laneous2.Gigs != null);
            Assert(laneous2.Gigs.Count() == 1);

            _engine.CreateMap<Artist>()
                .CollectionProperty(
                    x => x.Gigs,
                    id => new Node(1061).GetDescendantNodesByType("Gig").Select(x => x.Id)
                    );
            _engine.CreateMap<SoloArtist>()
                .RemoveMappingForProperty(x => x.Reviews);

            var laneous3 = _engine.Query<Artist>()
                .Include(x => x.Gigs)
                .Find(1060);

            Assert(laneous3.Gigs != null);
            Assert(laneous3.Gigs.Count() == 2);
            Assert(laneous3.Gigs.Any(x => x.Id == 1073));

            _engine.CreateMap<SoloArtist>()
                .CollectionProperty<string>(
                    x => x.Gigs,
                    p =>
                    {
                        Assert(p == "Lachlan");
                        return new[] { 1074, 1073 };
                    },
                    "firstName"
                    );

            var laneous4 = _engine.Query<Artist>()
                .Include(x => x.Gigs)
                .Find(1060);

            Assert(laneous4.Gigs != null);
            Assert(laneous4.Gigs.Count() == 2);
            Assert(laneous4.Gigs.Any(x => x.Id == 1073));
            Assert(laneous4.Gigs.Any(x => x.Id == 1074));
        }

        private void TestCustomPropertyMapping()
        {
            _engine.CreateMap<Gig>();

            var wasRun = false;
            _engine.CreateMap<Artist>()
                .RemoveMappingForProperty(x => x.Reviews)
                .CustomProperty(
                    x => x.Name,
                    (id, paths, cache) =>
                    {
                        if (_engine.IsCachingEnabled)
                        {
                            Assert(cache != null);
                        }

                        Assert(id == 1061);
                        wasRun = true;
                        return "Kafka";
                    },
                    false,
                    true
                    );

            var kooii = _engine.Query<Artist>()
                .Find(1061);

            Assert(kooii.Name == "Kafka");

            if (_engine.IsCachingEnabled)
            {
                wasRun = false;
                var kooiiCached = _engine.Query<Artist>()
                    .Find(1061);

                Assert(!wasRun);
            }

            var wasRun2 = false;
            _engine.CreateMap<Artist>()
                .CustomProperty(
                    x => x.Name,
                    (id, paths, cache) =>
                    {
                        Assert(paths.Single() == "Whatsup");
                        wasRun2 = true;
                        return "Yo";
                    },
                    true,
                    false
                    );

            var kooii2 = _engine.Query<Artist>()
                .Find(1061);

            Assert(kooii2.Name == null);

            var kooii3 = _engine.Query<Artist>()
                .Include("Name.Whatsup")
                .Find(1061);

            Assert(kooii3.Name == "Yo");

            if (_engine.IsCachingEnabled)
            {
                wasRun2 = false;

                var kooii2Cached = _engine.Query<Artist>()
                    .Include("Name.Whatsup")
                    .Find(1061);

                Assert(wasRun2); // caching is disabled for this property
            }
        }

        /* Need to reimplement the below */
        //private void TestPropertyMappingChange()
        //{
        //    _log.AppendLine("    Creating map...");

        //    var config = _mapper.CreateMap<StringsOnly>();

        //    var defaultModel = _mapper.Map<StringsOnly>(new Node(1045));

        //    config.ForProperty(x => x.PhoneNumber, n => n.UrlName, false);

        //    var modifiedModel = _mapper.Map<StringsOnly>(new Node(1045));

        //    Assert(modifiedModel.Name == "Strings Only");
        //    Assert(modifiedModel.PhoneNumber == "strings-only");

        //    _log.AppendLine("    Success!");
        //}

        //private void TestInvalidMapException()
        //{
        //    _log.AppendLine("    Creating map...");
        //    _mapper.CreateMap<SingleRelationshipInvalidCast>("SingleRelationship");

        //    bool didFailCorrectly = false;
        //    try
        //    {
        //        var model = _mapper.Get<SingleRelationshipInvalidCast>(1060, true);
        //    }
        //    catch (MapNotFoundException)
        //    {
        //        didFailCorrectly = true;
        //    }

        //    Assert(didFailCorrectly);

        //    _log.AppendLine("    Success!");
        //}

        //private void TestDefaultNodeProperties()
        //{
        //    _log.AppendLine("    Creating map...");
        //    _mapper.CreateMap<DefaultNodeProperties>("SystemTypesOnly");

        //    var node = new Node(1049);
        //    var model = _mapper.Map<DefaultNodeProperties>(node);

        //    Assert(model.CreateDate == new DateTime(2012, 10, 17, 17, 9, 21));
        //    Assert(model.CreatorID == 0);
        //    Assert(model.CreatorName == "admin");
        //    Assert(model.Id == 1049);
        //    Assert(model.Level == 1);
        //    Assert(model.Name == "System Types Only Full");
        //    Assert(model.NodeTypeAlias == "SystemTypesOnly");
        //    Assert(model.Path == "-1,1049");
        //    Assert(model.SortOrder == 3);
        //    Assert(model.template == 0);
        //    Assert(model.UpdateDate < DateTime.Now);
        //    Assert(model.Url == "/system-types-only-full/");
        //    Assert(model.UrlName == "system-types-only-full");
        //    Assert(model.Version == new Guid());
        //    Assert(model.WriterID == 0);
        //    Assert(model.WriterName == "admin");

        //    _log.AppendLine("    Success!");
        //}

        //private void TestNullMap()
        //{
        //    var model = _mapper.Map<StringsOnly>(null);

        //    Assert(model == null);

        //    _log.AppendLine("    Success!");
        //}

        //private void TestUnknownDoctype()
        //{
        //    var didFailCorrectly = false;

        //    try
        //    {
        //        _mapper.CreateMap<NotMapped>("Whatever");
        //    }
        //    catch (DocumentTypeNotFoundException)
        //    {
        //        didFailCorrectly = true;
        //    }

        //    Assert(didFailCorrectly);

        //    _log.AppendLine("    Success!");
        //}

        //private void TestMissingMap()
        //{
        //    var didFailCorrectly = false;
        // blahblahasdf

        //    try
        //    {
        //        _mapper.Map<NotMapped>(new Node(1061));
        //    }
        //    catch (MapNotFoundException)
        //    {
        //        didFailCorrectly = true;
        //    }

        //    Assert(didFailCorrectly);

        //    _log.AppendLine("    Success!");
        //}

        public void RunPerformanceTest(StringBuilder log, int iterations)
        {
            log.AppendLine("Starting performance tests...");

            var stopwatch = new Stopwatch();
            INodeMappingEngine engine;

            // No caching
            engine = GetEngineForPerformanceTests(false);
            engine.SetCacheProvider(null);

            stopwatch.Start();
            for (long i = 0; i < iterations; i++)
            {
                RunExpensiveEngineOperation(engine);
            }
            stopwatch.Stop();

            log.AppendLine(string.Format(
                "{0} iterations completed in {1} milliseconds caching disabled",
                iterations,
                stopwatch.ElapsedMilliseconds
                ));

            stopwatch.Reset();

            // Caching
            engine = GetEngineForPerformanceTests(true);

            // Warm cache
            RunExpensiveEngineOperation(engine);

            stopwatch.Start();
            for (long i = 0; i < iterations; i++)
            {
                RunExpensiveEngineOperation(engine);
            }
            stopwatch.Stop();

            log.AppendLine(string.Format(
                "{0} iterations completed in {1} milliseconds with caching enabled",
                iterations,
                stopwatch.ElapsedMilliseconds
                ));

            log.AppendLine("Finished performance tests");
        }

        private INodeMappingEngine GetEngineForPerformanceTests(bool withCaching)
        {
            var engine = new NodeMappingEngine();
            engine.SetCacheProvider(withCaching ? new DefaultCacheProvider(HttpContext.Current.Cache) : null);

            // Create maps
            engine.CreateMap<Homepage>("Site"); // override node type alias
            engine.CreateMap<Genre>();
            engine.CreateMap<Gig>();

            // Maps a uComponents "Data Type Grid" to a non-relational collection
            CustomPropertyMapping reviewsMapping = (id, paths, cache) =>
            {
                var node = new Node(id); // the node will definitely exist when this mapping is run

                var xml = node.GetProperty<string>("reviews");

                if (string.IsNullOrWhiteSpace(xml))
                {
                    return new List<Artist.Review>();
                }

                return XElement.Parse(xml)
                    .Elements("item")
                    .Select(item => new Artist.Review
                    {
                        Reviewer = item.Element("reviewer").Value,
                        Body = item.Element("body").Value
                    })
                    .ToList();
            };


            engine.CreateMap<Artist>()
                .CustomProperty(
                    x => x.Reviews, // choose the model property
                    reviewsMapping, // set a mapping
                    false, // whether the mapping is deemed a 'relationship' with other nodes
                    true // allow the result of this mapping to be cached
                    );

            // SoloArtist inherits from Artist, as do the corresponding node types.
            // This map will inherit Artist's property mapping for Reviews.
            engine.CreateMap<SoloArtist>();

            return engine;
        }

        private void RunExpensiveEngineOperation(INodeMappingEngine engine)
        {
            var home = engine.Query<Homepage>()
                .Include(x => x.FeaturedArtist.Genres)
                .Single();

            var artists = engine.Query<Artist>()
                .Include(x => x.Gigs)
                .Include(x => x.Genres)
                .ToList();

            var explicitArtists = engine.Query<Artist>()
                .Include(x => x.Gigs)
                .Include(x => x.Genres)
                .Explicit()
                .ToList();

            var genres = engine.Query<Genre>()
                .ToList();
        }
    }
}